<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgRest\TCA;

use SGalinski\SgRest\Service\RegistrationService;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * This class contains methods for usage within the TCA forms.
 */
class TcaProvider implements SingletonInterface {
	/**
	 * The method generate an array for an option list, that contains the registered access groups for the rest api.
	 *
	 * @param array $config
	 * @return void
	 */
	public function createAccessGroupItemList(array &$config): void {
		/** @var RegistrationService $registrationService */
		$registrationService = GeneralUtility::makeInstance(RegistrationService::class);
		$accessGroups = $registrationService->getAccessGroups();

		$selectOptions = [];
		foreach ($accessGroups as $apiKey => $accessGroup) {
			if ($accessGroup['accessGroupName'] !== 'Authentication') {
				$selectOptions[] = [
					'0' => $accessGroup['accessGroupName'],
					'1' => $apiKey
				];
			}
		}

		$config['items'] = $selectOptions;
	}
}
