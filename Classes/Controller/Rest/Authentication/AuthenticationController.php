<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgRest\Controller\Rest\Authentication;

use Psr\Http\Message\ResponseInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use SGalinski\SgRest\Controller\AbstractRestController;
use SGalinski\SgRest\Service\BearerTokenService;
use TYPO3\CMS\Core\Configuration\ExtensionConfiguration;
use TYPO3\CMS\Core\Log\LogLevel;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Frontend\Authentication\FrontendUserAuthentication;

/**
 * Class AuthenticationController
 */
class AuthenticationController extends AbstractRestController implements LoggerAwareInterface {
	use LoggerAwareTrait;

	/**
	 * @var  BearerTokenService
	 */
	protected $bearerTokenService;

	/**
	 * @param BearerTokenService $bearerTokenService
	 */
	public function injectBearerTokenService(BearerTokenService $bearerTokenService): void {
		$this->bearerTokenService = $bearerTokenService;
	}

	/**
	 * Checks if there is a logged in frontend user and gives out a token
	 *
	 * @throws \Exception
	 */
	public function postGetbearertokenAction(): ResponseInterface {
		$loggedInUser = $GLOBALS['TSFE']->fe_user->user;

		if ($loggedInUser !== NULL) {
			$accessGroups = $loggedInUser['tx_sgrest_access_groups'];

			// if the user doesn't have permission for any access group, no need for him to get a token
			if ($accessGroups !== '') {
				$extConf = GeneralUtility::makeInstance(ExtensionConfiguration::class)->get('sg_rest');

				$expirationTime = $extConf['tokenExpirationTime'];
				$expires = time() + $expirationTime;
				$userId = $loggedInUser['uid'];

				$payload = [
					'user' => $userId,
					'exp' => $expires,
				];

				$token = $this->bearerTokenService->encode($payload, 'HS256');

				if (((int) $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['sg_rest']['enableLogging']) === 1) {
					$this->logger->log(
						LogLevel::INFO,
						'User "' . $loggedInUser['username'] . '" [' . $loggedInUser['uid'] . '] successfully authenticated for a bearer token',
						[]
					);
				}

				// Clear the session of the logged-in user again
				if ($GLOBALS['TSFE']->fe_user instanceof FrontendUserAuthentication) {
					$GLOBALS['TSFE']->fe_user->removeSessionData();
					$GLOBALS['TSFE']->fe_user = NULL;
				}

				$this->returnData(['bearerToken' => $token]);

				return $this->htmlResponse();
			}

			if (((int) $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['sg_rest']['enableLogging']) === 1) {
				$this->logger->log(
					LogLevel::ERROR,
					'User "' . $loggedInUser['username'] . '" [' . $loggedInUser['uid'] . '] isn\'t allowed to access the API.',
					['user' => $loggedInUser]
				);
			}
		} elseif (((int) $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['sg_rest']['enableLogging']) === 1) {
			$this->logger->log(
				LogLevel::ERROR,
				'No authenticated user found while trying to retrieve a bearer token.',
				[]
			);
		}

		throw new \Exception(
			'Something went wrong in the authentication process. Please check the logs or contact your website provider for more information.',
			403
		);
	}
}
