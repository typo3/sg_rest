<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgRest\Controller;

use Exception;
use Psr\Http\Message\ResponseInterface;
use SGalinski\SgRest\Service\Authentication\AuthenticationServiceInterface;
use SGalinski\SgRest\Service\DataResolveService;
use SGalinski\SgRest\Service\RegistrationService;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Mvc\Controller\ActionController;
use TYPO3\CMS\Extbase\Mvc\View\JsonView;
use TYPO3\CMS\Extbase\Property\TypeConverter\PersistentObjectConverter;
use TYPO3Fluid\Fluid\View\ViewInterface;

/**
 * Class AbstractRestController
 *
 * @package SGalinski\SgRest\Controller
 */
abstract class AbstractRestController extends ActionController {
	/**
	 * @var JsonView
	 */
	protected $view;

	/**
	 * @var string
	 */
	protected $defaultViewObjectName = JsonView::class;

	/**
	 * @var array
	 */
	protected $viewFormatToObjectNameMap = [
		'json' => JsonView::class
	];

	/**
	 *
	 * @var DataResolveService
	 */
	protected $dataResolveService;

	/**
	 *
	 * @var RegistrationService
	 */
	protected $registrationService;

	/**
	 *
	 * @var AuthenticationServiceInterface
	 */
	protected $authenticationService;

	/**
	 * @var string
	 */
	protected $className = '';

	/**
	 * @var string
	 */
	protected $apiKey = '';

	/**
	 * Inject the RegistrationService
	 *
	 * @param RegistrationService $registrationService
	 */
	public function injectRegistrationService(RegistrationService $registrationService): void {
		$this->registrationService = $registrationService;
	}

	/**
	 * Inject the DataResolveService
	 *
	 * @param DataResolveService $dataResolveService
	 */
	public function injectDataResolveService(DataResolveService $dataResolveService): void {
		$this->dataResolveService = $dataResolveService;
	}

	/**
	 * AbstractRestController constructor.
	 *
	 * @param AuthenticationServiceInterface $authenticationService
	 */
	public function __construct(AuthenticationServiceInterface $authenticationService) {
		$this->authenticationService = $authenticationService;
	}

	/**
	 * Initializes the view before invoking an action method.
	 *
	 * This is a fix for the broken GenericViewResolver which is not able to resolve
	 * the request to a PHP based ViewInterface object.
	 *
	 * @see https://forge.typo3.org/issues/91989
	 *
	 * @param ViewInterface $view The view to be initialized
	 */
	public function initializeView(ViewInterface $view): void {
		if (isset($this->viewFormatToObjectNameMap[$this->request->getFormat()])) {
			$this->view = GeneralUtility::makeInstance($this->viewFormatToObjectNameMap[$this->request->getFormat()]);
			$this->view->assign('settings', $this->settings);
		}
	}

	/**
	 * Prepare the request parameter for the given property name. This is only called for properties of a type that
	 * inherits from AbstractEntity.
	 *
	 * @param string $parameterName
	 * @return void
	 * @throws Exception
	 */
	protected function processRequestParameter($parameterName): void {
		$entityName = $this->dataResolveService->getEntityNameByClassName($this->className);
		$identifier = 0;
		if ($parameterName === $entityName && $this->request->hasArgument('identifier')) {
			$identifier = (int) $this->request->getArgument('identifier');
		}

		if ($this->request->hasArgument($parameterName)) {
			$accessGroups = $this->registrationService->getAccessGroups();
			$parameters = $this->request->getArgument($parameterName);
			$allowedParameters = $accessGroups[$this->apiKey]['entities'][$entityName]['write'];
			$forbidden = (is_array($parameters) ? array_diff_key($parameters, array_flip($allowedParameters)) : []);

			if (count($forbidden)) {
				$forbiddenProperties = implode(',', array_keys($forbidden));
				$message = 'You are not allowed to write the sub properties: ' . $forbiddenProperties;
				throw new Exception($message, 403);
			}

			// Add __identity for PUT requests
			if ($identifier > 0) {
				$parameters['__identity'] = $identifier;
			}

			$this->request = $this->request->withArgument($parameterName, $parameters);
			$mappingConfiguration = $this->arguments[$parameterName]->getPropertyMappingConfiguration();
			$mappingConfiguration->forProperty('*')->allowAllProperties();
			$mappingConfiguration
				->setTypeConverterOption(
					PersistentObjectConverter::class,
					PersistentObjectConverter::CONFIGURATION_CREATION_ALLOWED,
					TRUE
				)
				->setTypeConverterOption(
					PersistentObjectConverter::class,
					PersistentObjectConverter::CONFIGURATION_MODIFICATION_ALLOWED,
					TRUE
				);
		} elseif ($identifier > 0 && !$this->request->hasArgument($parameterName)) {
			// Set entity as argument for e.g. GET requests
			$this->request = $this->request->withArgument($parameterName, $identifier);
		}
	}

	/**
	 * Error Handler
	 *
	 * @return ResponseInterface
	 */
	public function errorAction(): ResponseInterface {
		$validationResults = $this->arguments->validate();
		$errors = $validationResults->getFlattenedErrors();

		if (is_countable($errors) ? count($errors) : 0) {
			$message = 'The request could not be completed due to a conflict with the current state of the resource. ';

			$violatedProperties = $this->getViolatedProperties($errors);
			if ($violatedProperties !== '') {
				$message .= 'The following properties are invalid or not set: ' . $violatedProperties;
			}

			throw new \RuntimeException($message, 409);
		}

		return $this->htmlResponse();
	}

	/**
	 * The method gets an array of errors. The array keys are the path to the property.
	 * So the key is like entity.property.
	 *
	 * The method collects all properties and returns a string with the comma separated properties.
	 *
	 * @param array $errors
	 * @return string
	 */
	protected function getViolatedProperties(array $errors): string {
		$violatedProperties = array_keys($errors);

		$properties = [];
		foreach ($violatedProperties as $violatedProperty) {
			// $violatedProperty contains the path like entity.property
			$entityPropertyPair = GeneralUtility::trimExplode('.', $violatedProperty, TRUE);

			if (isset($entityPropertyPair[1])) {
				$properties[] = $entityPropertyPair[1];
			}
		}

		return implode(', ', $properties);
	}

	/**
	 * Assigns the given data parameter to the view (should be used for the JSON format)
	 *
	 * @param mixed $data
	 * @return void
	 */
	protected function returnData($data) {
		if ($this->view instanceof JsonView) {
			$this->view->setVariablesToRender(['data']);
		}

		$this->view->assign('data', $data);
	}
}
