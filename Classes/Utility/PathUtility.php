<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgRest\Utility;

use SGalinski\SgRest\Service\RegistrationService;
use TYPO3\CMS\Core\Imaging\ImageManipulation\CropVariantCollection;
use TYPO3\CMS\Core\Resource\Exception\ResourceDoesNotExistException;
use TYPO3\CMS\Core\Resource\FileReference as CoreFileReference;
use TYPO3\CMS\Core\Resource\ProcessedFile;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Domain\Model\FileReference;
use TYPO3\CMS\Extbase\DomainObject\AbstractEntity;

/**
 * This is a utility class for the analysis of the REST request uri-path.
 */
class PathUtility {
	/**
	 * This method analysis the given request segments.
	 *
	 * @param array $requestSegments
	 * @return array
	 * @throws \Exception
	 */
	public static function analyseRequestSegments(array $requestSegments): array {
		$processedSegments = [
			'apiKey' => '',
			'entity' => '',
			'identifier' => 0,
			'verb' => '',
			'parameter' => []
		];

		$apiKey = trim(array_shift($requestSegments));
		if ($apiKey !== '') {
			$processedSegments['apiKey'] = $apiKey;
		}

		$entity = trim(array_shift($requestSegments));
		$entity = GeneralUtility::underscoredToLowerCamelCase($entity);
		if (self::isEntitySegment($processedSegments['apiKey'], $entity)) {
			$processedSegments['entity'] = $entity;
		} else {
			throw new \RuntimeException('The requested path does not exist.', 404);
		}

		$thirdSegment = array_shift($requestSegments);
		if (is_numeric($thirdSegment)) {
			$processedSegments['identifier'] = (int) $thirdSegment;
		} else {
			$processedSegments['verb'] = $thirdSegment;
		}

		if ($processedSegments['verb'] === '' && count($requestSegments) > 0) {
			$processedSegments['verb'] = array_shift($requestSegments);
		}

		$processedSegments['verb'] = GeneralUtility::underscoredToLowerCamelCase($processedSegments['verb']);

		// adding additional parameters like page or limit
		$processedSegments['additionalParameters'] = self::parseAdditionalParameters($requestSegments);

		return $processedSegments;
	}

	/**
	 * Return the given url segments as key value pairs in an array.
	 *
	 * @param array $urlSegments
	 * @return array
	 */
	protected static function parseAdditionalParameters(array $urlSegments): array {
		$additionalParameters = [];

		foreach ($urlSegments as $index => $value) {
			$previousIndex = $index - 1;
			if (($index % 2) === 1 && isset($urlSegments[$previousIndex])) {
				$parameterName = GeneralUtility::underscoredToLowerCamelCase($urlSegments[$previousIndex]);
				$additionalParameters[$parameterName] = $value;
			}
		}

		return $additionalParameters;
	}

	/**
	 * Check if the given entity segments exists for the apiKey.
	 *
	 * @param string $apiKey
	 * @param string $entitySegment
	 * @return bool
	 */
	protected static function isEntitySegment($apiKey, $entitySegment): bool {
		/** @var RegistrationService $registrationService */
		$registrationService = GeneralUtility::makeInstance(RegistrationService::class);
		$accessGroup = $registrationService->getAccessGroupByApiKey($apiKey);
		return ($entitySegment !== '' && isset($accessGroup['entities'][$entitySegment]));
	}

	/**
	 * Return a api url like:
	 * https://{host}/{apiKey}/{entity}/{identifier}
	 *
	 * @param string $apiKey
	 * @param string $entity
	 * @param int $identifier
	 * @return string
	 * @throws \Exception
	 */
	public static function createRestEntityUrl($apiKey, $entity, $identifier): string {
		$baseUrl = self::createRestBaseUrl($apiKey, $entity);
		$identifier = (int) $identifier;

		if ($baseUrl === '' && $identifier <= 0) {
			$message = 'You are not allowed to access the sub property: ' . $entity;
			throw new \RuntimeException($message, 403);
		}

		return $baseUrl . '/' . $identifier;
	}

	/**
	 * Return a file url like:
	 * {scheme}://{host}/{fileUrl}
	 *
	 * @param AbstractEntity $file
	 * @param string $maximumWidth
	 * @param string $targetFileExtension
	 * @param string $cropVariant
	 * @return string
	 * @throws \Exception
	 */
	public static function createFileUrl(
		AbstractEntity $file,
		$maximumWidth = '2048',
		$targetFileExtension = '',
		$cropVariant = 'default'
	): string {
		if (!($file instanceof FileReference)) {
			return '';
		}

		$scheme = (GeneralUtility::getIndpEnv('TYPO3_SSL') ? 'https://' : 'http://');
		$host = GeneralUtility::getIndpEnv('HTTP_HOST') . '/';
		$baseUrl = $scheme . $host;
		$originalResource = $file->getOriginalResource();

		if ($baseUrl === '' && !$originalResource) {
			$message = 'You are not allowed to access the file.';
			throw new \InvalidArgumentException($message, 403);
		}

		$fileUrl = self::processFileReference($originalResource, $maximumWidth, $targetFileExtension, $cropVariant);

		if (!$fileUrl) {
			$fileUrl = $originalResource->getPublicUrl();
		}

		// remove leading and trailing slashes
		$baseUrl = rtrim($baseUrl, '/');
		$fileUrl = ltrim($fileUrl, '/');

		return $baseUrl . '/' . $fileUrl;
	}

	/**
	 * @param CoreFileReference $file
	 * @param string $maximumWidth
	 * @param string $targetFileExtension
	 * @param string $cropVariant
	 * @return string
	 */
	protected static function processFileReference(
		CoreFileReference $file,
		$maximumWidth = '2048',
		$targetFileExtension = '',
		$cropVariant = 'default'
	): string {
		if (\strlen($maximumWidth) <= 0) {
			return '';
		}

		try {
			$cropString = NULL;

			if ($file->hasProperty('crop') && $file->getProperty('crop')) {
				$cropString = $file->getProperty('crop');
			}

			$cropVariantCollection = CropVariantCollection::create((string) $cropString);
			$cropArea = $cropVariantCollection->getCropArea($cropVariant);
			$processingInstructions = [
				'maxWidth' => $maximumWidth,
				'crop' => $cropArea->isEmpty() ? NULL : $cropArea->makeAbsoluteBasedOnFile($file)
			];

			if ($targetFileExtension !== '') {
				$processingInstructions['fileExtension'] = $targetFileExtension;
			}

			// getPublicUrl CAN be NULL
			$fileUrl = $file->getOriginalFile()->process(
				ProcessedFile::CONTEXT_IMAGECROPSCALEMASK,
				$processingInstructions
			)
				->getPublicUrl();
			return $fileUrl ?: '';
		} catch (ResourceDoesNotExistException $e) {
			// thrown if file does not exist
		} catch (\UnexpectedValueException $e) {
			// thrown if a file has been replaced with a folder
		} catch (\RuntimeException $e) {
			// RuntimeException thrown if a file is outside of a storage
		} catch (\InvalidArgumentException $e) {
			// thrown if file storage does not exist
		}

		return '';
	}

	/**
	 * Return a basic api url like:
	 * https://{host}/{apiKey}/{entity}
	 *
	 * If you have some url redirects the REST base URI is maybe different and need url segments between the host and
	 * the apikey. You are able to adjust the base URI with the url pattern parameter.
	 *
	 * The url pattern should contain the marker HOST, APIKEY, ENTITY. The markers use handlebars. So a pattern
	 * looks like: {{HOST}}/{{APIKEY}}/{{ENTITY}}
	 *
	 * @param string $apiKey
	 * @param string $entity
	 * @param string $urlPattern
	 * @return string
	 * @throws \Exception
	 */
	public static function createRestBaseUrl($apiKey, $entity, $urlPattern = ''): string {
		/** @var RegistrationService $registrationService */
		$registrationService = GeneralUtility::makeInstance(RegistrationService::class);
		$accessGroup = $registrationService->getAccessGroupByApiKey($apiKey);
		$defaultUrlPattern = '{{HOST}}/{{APIKEY}}/{{ENTITY}}';
		$urlSegments = [
			'{{HOST}}' => GeneralUtility::getIndpEnv('HTTP_HOST'),
			'{{ENTITY}}' => GeneralUtility::camelCaseToLowerCaseUnderscored($entity),
			'{{APIKEY}}' => $apiKey
		];

		if ($entity === '' && !isset($accessGroup['entities'][$entity])) {
			$message = 'You are not allowed to access the sub property: ' . $entity;
			throw new \RuntimeException($message, 403);
		}

		$urlPattern = trim($urlPattern) === '' ? $defaultUrlPattern : $urlPattern;
		return 'https://' . str_replace(array_keys($urlSegments), array_values($urlSegments), $urlPattern);
	}
}
