<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgRest\Service;

use DomainException;
use TYPO3\CMS\Core\SingletonInterface;
use UnexpectedValueException;

/**
 * This class handles the JWT / Bearer Token generation and validation for the BearerAuthenticationService
 *
 * Class BearerTokenService
 *
 * @package SGalinski\SgRest\Service
 */
class BearerTokenService implements SingletonInterface {
	/**
	 * stores the local private key
	 *
	 * @var string
	 */
	protected $privateKey;

	/**
	 * JWT Header
	 *
	 * @var string
	 */
	protected $header;

	/**
	 * JWT payload
	 *
	 * @var string
	 */
	protected $payload;

	/**
	 * Allow the current timestamp to be specified.
	 * Useful for fixing a value within unit testing.
	 *
	 * Will default to PHP time() value if null.
	 *
	 * @var int
	 */
	public $timestamp;

	/**
	 * @var array
	 */
	protected $extensionConfiguration;

	/**
	 * BearerAuthorizationService constructor.
	 */
	public function __construct() {
		// get the encryption key
		$this->privateKey = $GLOBALS['TYPO3_CONF_VARS']['SYS']['encryptionKey'];

		$this->header = json_encode([
			'typ' => 'JWT',
			'alg' => 'HS256'
		], JSON_THROW_ON_ERROR);
	}

	/**
	 * Decodes and verifies the bearer token
	 *
	 * @param string $bearerToken
	 * @return boolean
	 * @throws \JsonException
	 */
	public function verifyToken($bearerToken): bool {
		if (!empty($bearerToken)) {
			$payload = $this->decodeToken($bearerToken, $this->privateKey);

			if ($payload !== NULL) {
				$expire = $payload->exp;

				if (!empty($expire) && $expire > 0 && time() < $expire) {
					return TRUE;
				}
			}
		}

		return FALSE;
	}

	/**
	 * Decodes a JWT string into a PHP object.
	 *
	 * @param string $jwt The JWT
	 * @param bool $verify Don't skip verification process
	 *
	 * @return object      The JWT's payload as a PHP object
	 * @throws \JsonException
	 */
	public function decodeToken($jwt, $verify = TRUE) {
		$tokenSegments = explode('.', $jwt);

		if (count($tokenSegments) !== 3) {
			throw new UnexpectedValueException('Wrong number of segments');
		}

		[$jwtHeaderEncoded, $jwtPayloadEncoded, $jwtSignatureEncoded] = $tokenSegments;

		if (NULL === ($header = $this->jsonDecode($this->urlsafeB64Decode($jwtHeaderEncoded)))) {
			throw new UnexpectedValueException('Invalid segment encoding');
		}

		if (NULL === $payload = $this->jsonDecode($this->urlsafeB64Decode($jwtPayloadEncoded))) {
			throw new UnexpectedValueException('Invalid segment encoding');
		}

		$sig = $this->urlsafeB64Decode($jwtSignatureEncoded);

		if ($verify) {
			if (empty($header->alg)) {
				throw new DomainException('Empty algorithm');
			}

			if ($sig !== $this->sign("$jwtHeaderEncoded.$jwtPayloadEncoded", $this->privateKey, $header->alg)) {
				throw new UnexpectedValueException('Signature verification failed');
			}
		}

		return $payload;
	}

	/**
	 * Converts and signs a PHP object or array into a JWT string.
	 *
	 * @param object|array $payload PHP object or array
	 * @param string $algo The signing algorithm. Supported algorithms are 'HS256', 'HS384' and 'HS512'
	 *
	 * @return string      A signed JWT
	 * @throws \JsonException
	 * @uses jsonEncode
	 * @uses urlsafeB64Encode
	 */
	public function encode($payload, $algo = 'HS256'): string {
		$header = ['typ' => 'JWT', 'alg' => $algo];

		$segments = [];
		$segments[] = $this->urlsafeB64Encode($this->jsonEncode($header));
		$segments[] = $this->urlsafeB64Encode($this->jsonEncode($payload));
		$signing_input = implode('.', $segments);

		$signature = $this->sign($signing_input, $this->privateKey, $algo);
		$segments[] = $this->urlsafeB64Encode($signature);

		return implode('.', $segments);
	}

	/**
	 * Sign a string with a given key and algorithm.
	 *
	 * @param string $message The message to sign
	 * @param string $key The secret key
	 * @param string $method The signing algorithm
	 * @return string          An encrypted message
	 * @throws DomainException Unsupported algorithm was specified
	 */
	public function sign($message, $key, $method = 'HS256'): string {
		$methods = [
			'HS256' => 'sha256',
			'HS384' => 'sha384',
			'HS512' => 'sha512',
		];

		if (empty($methods[$method])) {
			throw new DomainException('Algorithm not supported');
		}

		return hash_hmac($methods[$method], $message, $key, TRUE);
	}

	/**
	 * Decode a JSON string into a PHP object.
	 *
	 * @param string $input JSON string
	 *
	 * @return object          Object representation of JSON string
	 * @throws DomainException Provided string was invalid JSON
	 * @throws \JsonException
	 */
	public function jsonDecode($input) {
		$obj = json_decode($input, FALSE, 512, JSON_THROW_ON_ERROR);
		if ($obj === NULL && $input !== 'null') {
			throw new DomainException('Null result with non-null input');
		}

		return $obj;
	}

	/**
	 * Encode a PHP object into a JSON string.
	 *
	 * //todo check if object makes sense here
	 *
	 * @param object|array $input A PHP object or array
	 * @return string          JSON representation of the PHP object or array
	 * @throws \JsonException
	 */
	public function jsonEncode($input): string {
		$json = json_encode($input, JSON_THROW_ON_ERROR);
		if ($json === 'null' && $input !== NULL) {
			throw new DomainException('Null result with non-null input');
		}

		return $json;
	}

	/**
	 * Decode a string with URL-safe Base64.
	 *
	 * @param string $input A Base64 encoded string
	 *
	 * @return string A decoded string
	 */
	public function urlsafeB64Decode($input): string {
		$remainder = strlen($input) % 4;
		if ($remainder) {
			$padlen = 4 - $remainder;
			$input .= str_repeat('=', $padlen);
		}

		return base64_decode(strtr($input, '-_', '+/'));
	}

	/**
	 * Encode a string with URL-safe Base64.
	 *
	 * @param string $input The string you want encoded
	 *
	 * @return string The base64 encode of what you passed in
	 */
	public function urlsafeB64Encode($input): string {
		return str_replace('=', '', strtr(base64_encode($input), '+/', '-_'));
	}
}
