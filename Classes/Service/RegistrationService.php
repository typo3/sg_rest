<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgRest\Service;

use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\ExtensionUtility;

/**
 * Class for registering new REST access groups and entities.
 */
class RegistrationService implements SingletonInterface {
	/**
	 * @var array
	 */
	protected $accessGroups = [];

	/**
	 * Getter for access groups
	 *
	 * @return array
	 */
	public function getAccessGroups(): array {
		return $this->accessGroups;
	}

	/**
	 * Registers an access group with the corresponding entities for the REST API.
	 *
	 * @param string $apiKey Unique key for the access group
	 * @param string $extensionName Extension and vendor name like SGalinski.sg_rest
	 * @param string $accessGroupName Name of the access group (used for the backend)
	 * @param array $entitiesConfiguration Configuration array with all entities and permissions
	 * @return void
	 * @throws \Exception
	 */
	public function registerAccessGroup(
		$apiKey,
		$extensionName,
		$accessGroupName = '',
		array $entitiesConfiguration = []
	): void {
		$accessGroup = [];
		if ($apiKey === '') {
			throw new \InvalidArgumentException('The api key must not be empty', 1414666527);
		}

		$apiKey = trim($apiKey);
		$accessGroup['accessGroupName'] = (trim($accessGroupName) === '' ? $apiKey : trim($accessGroupName));
		$extensionIdentifier = $this->getExtensionIdentifier($extensionName);
		$accessGroup = array_merge($accessGroup, $extensionIdentifier);

		$accessGroup['entities'] = [];
		foreach ($entitiesConfiguration as $entity => $entityConfiguration) {
			if (isset($entityConfiguration['write'])) {
				$writeFields = GeneralUtility::trimExplode(',', $entityConfiguration['write'], TRUE);
			} else {
				$writeFields = [];
			}

			if (isset($entityConfiguration['read'])) {
				$readFields = GeneralUtility::trimExplode(',', $entityConfiguration['read'], TRUE);
			} else {
				$readFields = [];
			}

			$accessGroup['entities'][$entity] = [
				'write' => $writeFields,
				'read' => array_merge($writeFields, $readFields),
				'httpPermissions' => $this->getHttpPermissionsConfiguration($entityConfiguration),
				'classFQN' => $entityConfiguration['classFQN'] ?? ''
			];
		}

		$this->accessGroups[$apiKey] = $accessGroup;
		$this->configurePluginForAccessGroup($apiKey);
	}

	/**
	 * The method returns the http permissions for the given configuration. The default permission overrule the given
	 * permissions. So if the httpPermission key is not set we only use the default permissions.
	 *
	 * @param array $configuration
	 * @return array
	 */
	protected function getHttpPermissionsConfiguration(array $configuration): array {
		$defaultHttpPermissions = ['deleteForVerbs' => FALSE, 'putWithIdentifier' => FALSE,
			'patchWithIdentifier' => FALSE, 'postWithIdentifier' => FALSE];

		$httpPermissions = [];
		if (isset($configuration['httpPermissions']) && is_array($configuration['httpPermissions'])) {
			$httpPermissions = array_intersect_key($configuration['httpPermissions'], $defaultHttpPermissions);
		}

		$httpPermissions = filter_var_array($httpPermissions, FILTER_VALIDATE_BOOLEAN);
		if ($httpPermissions) {
			return $httpPermissions + $defaultHttpPermissions;
		}

		return $defaultHttpPermissions;
	}

	/**
	 * Register the access group as plugin.
	 *
	 * @param string $apiKey
	 * @return void
	 */
	protected function configurePluginForAccessGroup($apiKey): void {
		$accessGroup = $this->getAccessGroupByApiKey($apiKey);
		$vendorName = $accessGroup['vendorName'];
		$extensionName = $accessGroup['extensionName'];

		$controller = [];
		foreach ($accessGroup['entities'] as $entity => $configuration) {
			$controllerName = $configuration['classFQN'];
			$controller[$controllerName] = '';
		}

		ExtensionUtility::configurePlugin(
			$extensionName,
			'Rest' . $apiKey,
			$controller,
			$controller,
			ExtensionUtility::PLUGIN_TYPE_CONTENT_ELEMENT
		);
	}

	/**
	 * Method returns an array with the extension key, the extension name and the vendor name.
	 *
	 * - The extension key is the lowercase name with underscore e.g. sg_rest.
	 * - The extension name is the upperCamelCase representation used for namespaces e.g. SgRest.
	 * - The vendor name is also used for name spaces e.g. SGalinski.
	 *
	 * @param string $extensionName
	 * @return array
	 * @throws \Exception
	 */
	protected function getExtensionIdentifier($extensionName): array {
		if ($extensionName === '') {
			throw new \InvalidArgumentException('The extension name must not be empty', 1414666475);
		}

		$identifier = [
			'vendorName' => '',
			'extensionName' => '',
			'extensionKey' => ''
		];

		$delimiterPosition = strrpos($extensionName, '.');
		if ($delimiterPosition !== FALSE) {
			$identifier['vendorName'] = str_replace('.', '\\', substr($extensionName, 0, $delimiterPosition));
			$extensionName = substr($extensionName, $delimiterPosition + 1);
		}

		$underscored = strpos($extensionName, '_');
		if ($underscored) {
			$identifier['extensionKey'] = $extensionName;
			$identifier['extensionName'] = GeneralUtility::underscoredToUpperCamelCase($extensionName);
		} elseif ($extensionName !== '') {
			$identifier['extensionKey'] = GeneralUtility::camelCaseToLowerCaseUnderscored($extensionName);
			$identifier['extensionName'] = $extensionName;
		}

		return $identifier;
	}

	/**
	 * Checks if the given api key is still available.
	 *
	 * @param string $apiKey
	 * @return bool
	 */
	public function isApiKeyAvailable($apiKey): bool {
		return array_key_exists(trim($apiKey), $this->accessGroups);
	}

	/**
	 * Checks if the given entity is available for the api key.
	 *
	 * @param string $entity
	 * @param string $apiKey
	 * @return bool
	 */
	public function isEntityAvailable($entity, $apiKey): bool {
		return array_key_exists(trim($entity), $this->accessGroups[trim($apiKey)]['entities']);
	}

	/**
	 * Returns the access group configuration for the given apiKey.
	 *
	 * @param string $apiKey
	 * @return array
	 */
	public function getAccessGroupByApiKey($apiKey): array {
		return ($this->isApiKeyAvailable($apiKey) ? $this->accessGroups[$apiKey] : []);
	}

	/**
	 * Returns the http permissions array for the given entity.
	 *
	 * @param string $entity
	 * @param string $apiKey
	 * @return array
	 * @throws \Exception
	 */
	public function getHttpPermissionsForEntity($entity, $apiKey): array {
		if (!$this->isEntityAvailable($entity, $apiKey)) {
			throw new \RuntimeException('The given entity is not available', 500);
		}

		return $this->accessGroups[trim($apiKey)]['entities'][trim($entity)]['httpPermissions'];
	}
}
