<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgRest\Service\Authentication;

/**
 * Interface AuthenticationServiceInterface
 */
interface AuthenticationServiceInterface {
	/**
	 * Tries to authenticate a request with the given request headers
	 *
	 * @param array $requestHeaders
	 * @return bool Returns if the authentication was successful
	 */
	public function verifyRequest(array $requestHeaders): bool;

	/**
	 * Verify if the authenticated user has access to the given apikey.
	 *
	 * @param string $apiKey
	 * @return bool
	 */
	public function verifyUserAccess(string $apiKey): bool;
}
