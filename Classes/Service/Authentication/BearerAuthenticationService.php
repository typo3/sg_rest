<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgRest\Service\Authentication;

use Doctrine\DBAL\Exception;
use SGalinski\SgRest\Service\BearerTokenService;
use TYPO3\CMS\Core\Database\ConnectionPool;
use TYPO3\CMS\Core\Database\Query\Restriction\DeletedRestriction;
use TYPO3\CMS\Core\SingletonInterface;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * This Service handles the authentication for bearer tokens
 *
 * Class BearerAuthenticationService
 *
 * @package SGalinski\SgRest\Service\Authentication
 */
class BearerAuthenticationService extends AbstractAuthenticationService implements SingletonInterface {
	/**
	 * @var BearerTokenService
	 */
	protected $bearerTokenService;

	/**
	 * @param BearerTokenService $bearerTokenService
	 */
	public function injectBearerTokenService(BearerTokenService $bearerTokenService): void {
		$this->bearerTokenService = $bearerTokenService;
	}

	/**
	 * @param array $requestHeaders
	 * @return bool
	 * @throws Exception
	 */
	public function verifyRequest(array $requestHeaders): bool {
		$bearerToken = $requestHeaders['bearertoken'][0] ?? '';
		return ($bearerToken !== '' && $this->verifyBearerToken($bearerToken));
	}

	/**
	 * @param string $bearerToken
	 * @return bool
	 * @throws Exception
	 */
	protected function verifyBearerToken($bearerToken): bool {
		$verified = $this->bearerTokenService->verifyToken($bearerToken);

		if ($verified) {
			$decodedToken = $this->bearerTokenService->decodeToken($bearerToken);

			if ($decodedToken && $decodedToken->user > 0) {
				$uid = $decodedToken->user;

				$queryBuilder = GeneralUtility::makeInstance(ConnectionPool::class)->getQueryBuilderForTable(
					'fe_users'
				);
				$queryBuilder->getRestrictions()->removeAll()->add(
					GeneralUtility::makeInstance(DeletedRestriction::class)
				);

				$user = $queryBuilder->select('*')
					->from('fe_users')->where($queryBuilder->expr()->eq(
						'uid',
						$queryBuilder->createNamedParameter($uid, \PDO::PARAM_INT)
					))->executeQuery()
					->fetchAllAssociative()[0];

				if (!empty($user)) {
					$this->authenticatedUser = $user;
					return TRUE;
				}
			}
		}

		return FALSE;
	}
}
