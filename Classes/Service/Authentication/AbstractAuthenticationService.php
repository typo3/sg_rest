<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgRest\Service\Authentication;

use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use SGalinski\SgRest\Service\RegistrationService;
use TYPO3\CMS\Core\Utility\GeneralUtility;

/**
 * Class AbstractAuthenticationService
 *
 * @package SGalinski\SgRest\Service\Authentication
 */
abstract class AbstractAuthenticationService implements AuthenticationServiceInterface, LoggerAwareInterface {
	use LoggerAwareTrait;

	/**
	 * @var array
	 */
	protected $authenticatedUser;

	/**
	 *
	 * @var RegistrationService
	 */
	protected $registrationService;

	/**
	 * @param RegistrationService $registrationService
	 */
	public function injectRegistrationService(RegistrationService $registrationService): void {
		$this->registrationService = $registrationService;
	}

	/**
	 * Verify if the authenticated user has access to the given apikey.
	 *
	 * @param string $apiKey
	 * @return bool
	 */
	public function verifyUserAccess($apiKey): bool {
		$verifyAccess = FALSE;

		if ($this->authenticatedUser) {
			$accessGroups = GeneralUtility::trimExplode(',', $this->authenticatedUser['tx_sgrest_access_groups'], TRUE);
			$verifyAccess = in_array($apiKey, $accessGroups, TRUE);
		}

		return $verifyAccess;
	}

	/**
	 * Getter for the authenticated user.
	 *
	 * @return array|NULL
	 */
	public function getAuthenticatedUser(): ?array {
		return $this->authenticatedUser;
	}
}
