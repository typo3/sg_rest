<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgRest\Service;

use SGalinski\SgRest\Utility\PathUtility;

/**
 * Service class for pagination of REST requests.
 */
class PaginationService {
	/**
	 * @var array
	 */
	protected $paginationSettings = [];

	/**
	 * @var int
	 */
	protected $defaultLimit = 0;

	/**
	 * @var string
	 */
	protected $apiKey = '';

	/**
	 * @var string
	 */
	protected $urlPattern = '';

	/**
	 * Construct for pagination service.
	 *
	 * @param int $page
	 * @param int $limit
	 * @param int $defaultLimit
	 * @param int $amountOfEntries
	 * @param string $apiKey
	 */
	public function __construct($page, $limit, $defaultLimit, $amountOfEntries, $apiKey) {
		$this->setPaginationSettings($page, $limit, $amountOfEntries);
		$this->setDefaultLimit($defaultLimit);
		$this->setApiKey($apiKey);
	}

	/**
	 * Getter for pageSettings
	 *
	 * @return array
	 */
	public function getPaginationSettings(): array {
		return $this->paginationSettings;
	}

	/**
	 * Returns an array with all relevant information for the pagination.
	 *
	 * @param int $page
	 * @param int $limit
	 * @param int $amountOfEntries
	 * @return void
	 */
	public function setPaginationSettings($page, $limit, $amountOfEntries): void {
		$this->paginationSettings = [
			'maximumOfPages' => $this->getMaximumOfPages($amountOfEntries, $limit),
			'page' => $this->getValidPageNumber($page, $limit, $amountOfEntries),
			'offset' => $this->getOffset($page, $limit, $amountOfEntries),
			'limit' => $limit,
			'amountOfEntries' => $amountOfEntries
		];
	}

	/**
	 * Setter for default limit
	 *
	 * @param int $limit
	 * @return void
	 */
	public function setDefaultLimit($limit): void {
		$this->defaultLimit = $limit;
	}

	/**
	 * Setter for the api key
	 *
	 * @param string $apiKey
	 * @return void
	 */
	public function setApiKey($apiKey): void {
		$this->apiKey = $apiKey;
	}

	/**
	 * Setter for the url pattern
	 *
	 * The url pattern should contain the marker HOST, APIKEY, ENTITY, SCHEME. The markers use handlebars. So a pattern
	 * looks like: {{SCHEME}}{{HOST}}/{{APIKEY}}/{{ENTITY}}
	 *
	 * @param string $pattern
	 * @return void
	 */
	public function setUrlPattern($pattern): void {
		$this->urlPattern = $pattern;
	}

	/**
	 * Return a valid page number.
	 *
	 * @param int $page
	 * @param int $limit
	 * @param int $amountOfEntries
	 * @return int
	 */
	protected function getValidPageNumber($page, $limit, $amountOfEntries): int {
		$maximumOfPages = $this->getMaximumOfPages($amountOfEntries, $limit);
		$page = ($page < 1 ? 1 : $page);
		return ($page > $maximumOfPages ? $maximumOfPages : $page);
	}

	/**
	 * Get the maximum number of pages.
	 *
	 * @param int $amountOfEntries
	 * @param int $limit
	 * @return int
	 */
	protected function getMaximumOfPages($amountOfEntries, $limit): int {
		$maximumOfPages = ceil($amountOfEntries / $limit);
		return (max($maximumOfPages, 1));
	}

	/**
	 * Calculates the offset.
	 *
	 * @param int $page
	 * @param int $limit
	 * @param int $amountOfEntries
	 * @return int
	 */
	protected function getOffset($page, $limit, $amountOfEntries) {
		$page = $this->getValidPageNumber($page, $limit, $amountOfEntries);
		return ($page - 1) * $limit;
	}

	/**
	 * Method returns a url to the previous page or null if this page is not valid. The returned URL optionally contains
	 * a limit parameter pair. This depends on the limit value in the paginationSettings. If this value differed from
	 * the default limit value, we also need to add a limit to the url.
	 *
	 * @param string $entityName
	 * @param string $actionName
	 * @param array $additionalParameters
	 * @return NULL|string
	 * @throws \Exception
	 */
	public function getPreviousPageUrl($entityName, $actionName = 'list', array $additionalParameters = []): ?string {
		if ($this->paginationSettings['page'] <= 1) {
			return NULL;
		}

		$additionalParameters['limit'] = (int) $this->paginationSettings['limit'];
		$additionalParameters = $this->processUrlParameters($additionalParameters);
		$additionalParameters['page'] = $this->paginationSettings['page'] - 1;
		$urlSegments = $this->buildUrlSegments($additionalParameters);

		$restBaseUrl = PathUtility::createRestBaseUrl($this->apiKey, $entityName, $this->urlPattern);
		$restBaseUrl .= '/' . $actionName . $urlSegments;
		return $restBaseUrl;
	}

	/**
	 * Method returns a url to the previous page or null if this page is not valid. The returned URL optionally contains
	 * a limit parameter pair. This depends on the limit value in the paginationSettings. If this value differed from
	 * the default limit value, we also need to add a limit to the url.
	 *
	 * @param string $entityName
	 * @param string $actionName
	 * @param array $additionalParameters
	 * @return NULL|string
	 * @throws \Exception
	 */
	public function getNextPageUrl($entityName, $actionName = 'list', array $additionalParameters = []): ?string {
		if ($this->paginationSettings['page'] >= $this->paginationSettings['maximumOfPages']) {
			return NULL;
		}

		$additionalParameters['limit'] = (int) $this->paginationSettings['limit'];
		$additionalParameters = $this->processUrlParameters($additionalParameters);
		$additionalParameters['page'] = $this->paginationSettings['page'] + 1;
		$urlSegments = $this->buildUrlSegments($additionalParameters);

		$restBaseUrl = PathUtility::createRestBaseUrl($this->apiKey, $entityName, $this->urlPattern);
		$restBaseUrl .= '/' . $actionName . $urlSegments;
		return $restBaseUrl;
	}

	/**
	 * This method cleans up the url parameter for the previous and next url.
	 * The apiKey should not be inside and the default limit is also useless.
	 *
	 * @param array $parameters
	 * @return array
	 */
	protected function processUrlParameters(array $parameters): array {
		if (array_key_exists('apiKey', $parameters)) {
			unset($parameters['apiKey']);
		}

		if (array_key_exists('limit', $parameters) && (int) $parameters['limit'] === $this->defaultLimit) {
			unset($parameters['limit']);
		}

		return $parameters;
	}

	/**
	 * Creates url segments of the key value pairs of the given parameter.
	 *
	 * @param array $parameters
	 * @return string
	 */
	protected function buildUrlSegments(array $parameters): string {
		$urlSegments = array_map(
			static function ($value, $key) {
				return $key . '/' . urlencode($value);
			},
			array_values($parameters),
			array_keys($parameters)
		);
		return '/' . implode('/', $urlSegments);
	}
}
