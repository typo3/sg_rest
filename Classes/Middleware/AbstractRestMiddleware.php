<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgRest\Middleware;

use Exception;
use Psr\Http\Server\MiddlewareInterface;
use Psr\Log\LoggerAwareInterface;
use Psr\Log\LoggerAwareTrait;
use SGalinski\SgRest\Service\Authentication\AuthenticationServiceInterface;
use SGalinski\SgRest\Service\Authentication\BasicAuthenticationService;
use SGalinski\SgRest\Service\Authentication\BearerAuthenticationService;
use SGalinski\SgRest\Service\RegistrationService;
use TYPO3\CMS\Core\Http\Response;

/**
 * This class provides the basic prerequisites for the REST middlewares. New middlewares ideally extend this class
 *
 * Class AbstractRestMiddleware
 *
 * @package SGalinski\SgRest\Middleware
 */
abstract class AbstractRestMiddleware implements LoggerAwareInterface, MiddlewareInterface {
	use LoggerAwareTrait;

	/**
	 * The namespace of the sg_rest parameters
	 */
	public const argumentNamespace = 'tx_sgrest';

	/**
	 * The page type of the rest request
	 */
	public const restPageType = 1595576052;

	/**
	 * Array of Exception Codes that we will return as proper JSON response
	 *
	 * @var int[]
	 */
	protected $reasonableExceptionCodes = [
		401, 403, 404
	];

	/**
	 * This array contains all information about the request header as associative array.
	 *
	 * @var array
	 */
	protected $requestHeaders = [];

	/**
	 * @var array
	 */
	protected $requestSegments = [];

	/**
	 * @var array
	 */
	protected $pathSegments = [];

	/**
	 * @var array
	 */
	protected $accessGroup = [];

	/**
	 * @var object|BasicAuthenticationService|BearerAuthenticationService
	 */
	protected $authenticationService;

	/**
	 * @var RegistrationService
	 */
	protected $registrationService;

	/**
	 * @param RegistrationService $registrationService
	 */
	public function injectRegistrationService(RegistrationService $registrationService): void {
		$this->registrationService = $registrationService;
	}

	/**
	 * AbstractRestMiddleware constructor.
	 *
	 * @param AuthenticationServiceInterface $authenticationService
	 */
	public function __construct(AuthenticationServiceInterface $authenticationService) {
		$this->authenticationService = $authenticationService;
	}

	/**
	 * Returns the action name
	 *
	 * @return string
	 * @throws Exception
	 */
	protected function getActionName(): string {
		return $this->pathSegments['verb'];
	}

	/**
	 * Method generates the class name for the requested rest controller.
	 *
	 * @return string
	 * @throws Exception
	 */
	protected function getClassName(): string {
		$className = '';
		if (isset($this->accessGroup['vendorName'])) {
			$className .= $this->accessGroup['vendorName'] . '\\';
		}

		if (isset($this->accessGroup['extensionName'])) {
			$className .= $this->accessGroup['extensionName'] . '\\';
		}

		if (isset($this->pathSegments['entity'])) {
			$className .= 'Controller\\' . $this->getControllerName() . 'Controller';
		}

		return $className;
	}

	/**
	 * Method generates the class name for the requested rest controller.
	 *
	 * @return string
	 * @throws Exception
	 */
	protected function getControllerName(): string {
		$controllerName = '';

		if (isset($this->pathSegments['entity'])) {
			$controllerName .= 'Rest\\' . ucfirst($this->pathSegments['apiKey']) . '\\' . ucfirst(
				$this->pathSegments['entity']
			);
		}

		return $controllerName;
	}

	/**
	 * Method returns the requested action for the rest controller. If no
	 *
	 * @param string $method
	 * @return string
	 */
	protected function getCallableActionName($method): string {
		if ($method === 'POST') {
			return 'post' . mb_strtoupper(mb_substr($this->pathSegments['verb'], 0, 1)) . mb_substr(
				$this->pathSegments['verb'],
				1
			);
		}
		return 'get' . mb_strtoupper(mb_substr($this->pathSegments['verb'], 0, 1)) . mb_substr(
			$this->pathSegments['verb'],
			1
		);
	}

	/**
	 * @param mixed $data
	 * @param int $statusCode
	 * @return Response
	 * @throws \JsonException
	 */
	protected function createExceptionJsonResponse($data, int $statusCode): Response {
		if (is_string($data)) {
			$message = $data;
			$data = [];
			$data['message'] = $message;
		}
		$response = (new Response())
			->withStatus($statusCode)
			->withHeader('Content-Type', 'application/json; charset=utf-8');

		if (!empty($data)) {
			$options = JSON_HEX_TAG | JSON_HEX_APOS | JSON_HEX_AMP | JSON_HEX_QUOT | JSON_UNESCAPED_SLASHES;
			$response->getBody()->write(json_encode($data ?: NULL, JSON_THROW_ON_ERROR | $options));
			$response->getBody()->rewind();
		}

		return $response;
	}
}
