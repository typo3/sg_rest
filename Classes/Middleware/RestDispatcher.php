<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgRest\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use SGalinski\SgRest\Service\DataResolveService;
use SGalinski\SgRest\Utility\PathUtility;
use TYPO3\CMS\Core\Context\Context;
use TYPO3\CMS\Core\Context\TypoScriptAspect;
use TYPO3\CMS\Core\Http\Response;
use TYPO3\CMS\Core\Log\LogLevel;
use TYPO3\CMS\Core\Log\LogManager;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Configuration\FrontendConfigurationManager;
use TYPO3\CMS\Frontend\Page\CacheHashCalculator;

/**
 * Dispatches the already authenticated REST request
 *
 * Class RestDispatcher
 *
 * @package SGalinski\SgRest\Middleware
 */
class RestDispatcher extends AbstractRestMiddleware {
	/**
	 * Process an incoming server request.
	 *
	 * Processes an incoming server request in order to produce a response.
	 * If unable to produce the response itself, it may delegate to the provided
	 * request handler to do so.
	 *
	 * @param ServerRequestInterface $request
	 * @param RequestHandlerInterface $handler
	 * @return ResponseInterface
	 * @throws \Exception
	 */
	public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface {
		$queryParams = $request->getQueryParams();

		if (!isset($queryParams[self::argumentNamespace])) {
			return $handler->handle($request);
		}

		$parsedBody = $request->getParsedBody();

		$this->requestSegments = GeneralUtility::trimExplode(
			'/',
			$queryParams[self::argumentNamespace]['request'],
			TRUE
		);
		$this->requestHeaders = $request->getHeaders();

		// Get class name and the action name of the requested rest controller
		try {
			$this->pathSegments = PathUtility::analyseRequestSegments($this->requestSegments);
		} catch (\RuntimeException $exception) {
			if (in_array($exception->getCode(), $this->reasonableExceptionCodes, TRUE)) {
				return $this->createExceptionJsonResponse($exception->getMessage(), $exception->getCode());
			}

			throw $exception;
		}

		$apiKey = $this->pathSegments['apiKey'];
		$identifier = $this->pathSegments['identifier'];
		$this->accessGroup = $this->registrationService->getAccessGroupByApiKey($apiKey);

		$format = 'json';
		$vendorName = $this->accessGroup['vendorName'];
		$extensionName = $this->accessGroup['extensionName'];

		$className = $this->getClassName();
		$controllerName = $this->getControllerName();
		$httpMethod = $request->getMethod();
		$actionName = $this->getCallableActionName($httpMethod);

		/** @var DataResolveService $dataResolveService */
		$dataResolveService = GeneralUtility::makeInstance(DataResolveService::class);
		$dataResolveService->setApiKey($apiKey);
		if (isset($this->requestHeaders['resolveresourcelinks'])) {
			$resolveResourceLinks = filter_var($this->requestHeaders['resolveresourcelinks'], FILTER_VALIDATE_BOOLEAN);
			$dataResolveService->setResolveResourceLinks($resolveResourceLinks);
		}

		if (isset($this->requestHeaders['resolvenestinglevel'])) {
			$resolveNestingLevel = (int) $this->requestHeaders['resolvenestinglevel'];
			$dataResolveService->setResolveNestingLevel($resolveNestingLevel);
		}

		$site = $request->getAttribute('site');
		$queryParams['id'] = $site->getRootPageId();

		/**
		 * Set the missing plugin configuration
		 */
		$typoscriptPluginConfiguration = 'sgRest.10.extensionName=' . $extensionName . PHP_EOL
			. 'sgRest.10.controller=' . $controllerName . PHP_EOL
			. 'sgRest.10.action=' . $actionName . PHP_EOL
			. 'sgRest.10.pluginName=Rest' . $apiKey . PHP_EOL;

		# Set Action Name in $GLOBALS, will be read from \TYPO3\CMS\Extbase\Configuration\FrontendConfigurationManager::getControllerConfiguration
		# to determine currently allowed Controller::Actions
		$controllerConfiguration = [
			'className' => $className,
			'alias' => $controllerName,
			'actions' => [
				$actionName
			]
		];

		$GLOBALS['TYPO3_CONF_VARS']['EXTCONF']['extbase']['extensions'][$extensionName]['plugins']['Rest' . $apiKey]['controllers'][$className] = $controllerConfiguration;

		ExtensionManagementUtility::addTypoScriptSetup($typoscriptPluginConfiguration);
		$pluginNamespace = 'tx_' . strtolower($extensionName) . '_rest';

		if (PHP_VERSION_ID > 70000 && $httpMethod === 'POST') {
			$postparameters = json_decode(file_get_contents('php://input'), TRUE);
		} else {
			$postparameters = $parsedBody[self::argumentNamespace]['parameters'] ?? $queryParams[self::argumentNamespace]['parameters'] ?? [];
		}

		$parsedBody[$pluginNamespace . $apiKey] = $postparameters;
		unset($parsedBody[self::argumentNamespace]);

		$request = $request->withParsedBody($parsedBody);
		if (isset($this->pathSegments['additionalParameters'])) {
			foreach ($this->pathSegments['additionalParameters'] as $key => $value) {
				$queryParams[$pluginNamespace . $apiKey][$key] = $value;
			}
		}

		$queryParams[$pluginNamespace]['controller'] = $controllerName;
		$queryParams[$pluginNamespace]['action'] = $actionName;
		$queryParams[$pluginNamespace]['format'] = $format;
		$queryParams[$pluginNamespace]['extensionName'] = $extensionName;
		$queryParams[$pluginNamespace]['extension'] = $extensionName;
		$queryParams[$pluginNamespace]['vendor'] = $vendorName;
		$queryParams[$pluginNamespace]['vendorName'] = $vendorName;

		# Add both cases, with and without apiKey, since we seem to have inconsistencies with usual and bearerToken calls
		$queryParams[$pluginNamespace . $apiKey]['controller'] = $controllerName;
		$queryParams[$pluginNamespace . $apiKey]['action'] = $actionName;
		$queryParams[$pluginNamespace . $apiKey]['format'] = $format;
		$queryParams[$pluginNamespace . $apiKey]['extensionName'] = $extensionName;
		$queryParams[$pluginNamespace . $apiKey]['extension'] = $extensionName;
		$queryParams[$pluginNamespace . $apiKey]['vendor'] = $vendorName;
		$queryParams[$pluginNamespace . $apiKey]['vendorName'] = $vendorName;

		if ($identifier) {
			$queryParams[$pluginNamespace . $apiKey]['identifier'] = $identifier;
		}

		unset($queryParams[self::argumentNamespace]);

		// we will need a chash, because if we do not give one these will
		// all get the same INT_ script+configuration from $pageSectionCacheContent in TSFE.
		// in cases where we have different plugins on same page, only one will end up as INT_ in Cache.
		$cacheHashCalculator = GeneralUtility::makeInstance(CacheHashCalculator::class);
		$newCHash = $cacheHashCalculator->calculateCacheHash($queryParams[$pluginNamespace]);
		$queryParams['cHash'] = $newCHash;

		$request = $request->withQueryParams($queryParams);
		GeneralUtility::makeInstance(Context::class)->setAspect(
			'typoscript',
			GeneralUtility::makeInstance(TypoScriptAspect::class, TRUE)
		);

		if (((int) $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['sg_rest']['enableLogging']) === 1) {
			$this->logger->log(
				LogLevel::INFO,
				'REST Request before runControllerAction: ',
				[
					'httpMethod' => $httpMethod,
					'identifier' => $identifier,
					'parameters' => $postparameters,
					'apiKey' => $apiKey,
					'requestSegments' => $this->requestSegments,
					'requestHeaders' => $this->requestHeaders,
					'className' => $className,
					'actionName' => $actionName
				]
			);
		}

		try {
			return $handler->handle($request);
		} catch (\Exception $exception) {
			return $this->getErrorResponse($exception, (int) $exception->getCode());
		}
	}

	/**
	 * Builds an Error Response from caught exception
	 *
	 * @param \Exception $exception
	 * @param int $statusCode
	 * @return Response
	 * @throws \Exception
	 */
	private function getErrorResponse(\Exception $exception, int $statusCode): Response {
		if (((int) $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['sg_rest']['enableLogging']) === 1) {
			$logger = GeneralUtility::makeInstance(LogManager::class)->getLogger(self::class);
			$logger->critical(
				'AbstractAjaxController::processRequest - Request failed with an exception',
				[
					'message' => $exception->getMessage(),
					'code' => $exception->getCode(),
					'line' => $exception->getLine(),
					'trace' => $exception->getTraceAsString()
				]
			);
		}

		try {
			$response = new Response('php://temp', $statusCode ?: 500);
		} catch (\InvalidArgumentException $e) {
			throw $exception;
		}
		$body = $response->getBody();
		if ($body) {
			$data['message'] = $exception->getMessage();
			$body->write(json_encode($data));
			$body->rewind();
		}

		// we must throw the HTTP error code (in production context, this isn't always delivered correctly. At least in TYPO3 10.)
		header(
			'HTTP/' . $response->getProtocolVersion() . ' ' .
			$response->getStatusCode() . ' ' . $response->getReasonPhrase()
		);

		return $response;
	}
}
