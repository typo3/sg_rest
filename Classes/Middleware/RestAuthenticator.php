<?php

/***************************************************************
 *  Copyright notice
 *
 *  (c) sgalinski Internet Services (https://www.sgalinski.de)
 *
 *  All rights reserved
 *
 *  This script is part of the TYPO3 project. The TYPO3 project is
 *  free software; you can redistribute it and/or modify
 *  it under the terms of the GNU General Public License as published by
 *  the Free Software Foundation; either version 3 of the License, or
 *  (at your option) any later version.
 *
 *  The GNU General Public License can be found at
 *  http://www.gnu.org/copyleft/gpl.html.
 *
 *  This script is distributed in the hope that it will be useful,
 *  but WITHOUT ANY WARRANTY; without even the implied warranty of
 *  MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE.  See the
 *  GNU General Public License for more details.
 *
 *  This copyright notice MUST APPEAR in all copies of the script!
 ***************************************************************/

namespace SGalinski\SgRest\Middleware;

use Psr\Http\Message\ResponseInterface;
use Psr\Http\Message\ServerRequestInterface;
use Psr\Http\Server\RequestHandlerInterface;
use Psr\Log\LogLevel;
use SGalinski\SgRest\Utility\PathUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Core\Utility\HttpUtility;

/**
 * Authenticates incoming REST requests
 *
 * Class RestAuthenticator
 *
 * @package SGalinski\SgRest\Middleware
 */
class RestAuthenticator extends AbstractRestMiddleware {
	/**
	 * Process an incoming server request.
	 *
	 * Processes an incoming server request in order to produce a response.
	 * If unable to produce the response itself, it may delegate to the provided
	 * request handler to do so.
	 *
	 * @param ServerRequestInterface $request
	 * @param RequestHandlerInterface $handler
	 * @return ResponseInterface
	 * @throws \Exception
	 */
	public function process(ServerRequestInterface $request, RequestHandlerInterface $handler): ResponseInterface {
		$queryParams = $request->getQueryParams();

		if (!isset($queryParams[self::argumentNamespace])) {
			return $handler->handle($request);
		}

		$this->requestSegments = GeneralUtility::trimExplode(
			'/',
			$queryParams[self::argumentNamespace]['request'],
			TRUE
		);
		$this->requestHeaders = $request->getHeaders();

		// Get class name and the action name of the requested rest controller
		try {
			$this->pathSegments = PathUtility::analyseRequestSegments($this->requestSegments);
		} catch (\RuntimeException $exception) {
			if (in_array($exception->getCode(), $this->reasonableExceptionCodes, TRUE)) {
				return $this->createExceptionJsonResponse($exception->getMessage(), $exception->getCode());
			}
			throw $exception;
		}

		$apiKey = $this->pathSegments['apiKey'];
		$identifier = $this->pathSegments['identifier'];
		try {
			// @todo: this currently returns a 500 for a permission question?
			$httpPermissions = $this->registrationService->getHttpPermissionsForEntity(
				$this->pathSegments['entity'],
				$apiKey
			);
		} catch (\RuntimeException $exception) {
			if (in_array($exception->getCode(), $this->reasonableExceptionCodes, TRUE)) {
				return $this->createExceptionJsonResponse($exception->getMessage(), $exception->getCode());
			}

			throw $exception;
		}

		$this->accessGroup = $this->registrationService->getAccessGroupByApiKey($apiKey);

		$className = $this->getClassName();

		$httpMethod = $request->getMethod();
		$actionName = $this->getCallableActionName($httpMethod);

		/**
		 * when the client requests a bearer token, we don't need to do access checks etc. the user verification is done by the AuthServices
		 */
		if (!($this->pathSegments['entity'] === 'authentication' && $actionName === 'postGetbearertoken' && $httpMethod === 'POST')) {
			$authenticated = $this->authenticationService->verifyRequest($this->requestHeaders);

			if (!$authenticated) {
				if (((int) $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['sg_rest']['enableLogging']) === 1) {
					$this->logger->log(
						LogLevel::ERROR,
						'Auth-token was not provided or was invalid.',
						[
							'requestSegments' => $this->requestSegments,
							'requestHeaders' => $this->requestHeaders
						]
					);
				}

				return $this->createExceptionJsonResponse('Auth-token was not provided or was invalid.', 401);
			}

			$apiKey = $this->requestSegments[0];
			$verifiedAccess = $this->authenticationService->verifyUserAccess($apiKey);

			if (!$verifiedAccess) {
				if (((int) $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['sg_rest']['enableLogging']) === 1) {
					$this->logger->log(
						LogLevel::ERROR,
						'You tried to access an object where you do not have the necessary permissions.',
						[
							'apiKey' => $apiKey,
							'requestSegments' => $this->requestSegments,
							'requestHeaders' => $this->requestHeaders
						]
					);
				}

				return $this->createExceptionJsonResponse(
					'You tried to access an object where you do not have the necessary permissions.',
					403
				);
			}

			if ($httpMethod === 'DELETE' && $this->pathSegments['verb'] !== '' && !$httpPermissions['deleteForVerbs']) {
				if (((int) $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['sg_rest']['enableLogging']) === 1) {
					$this->logger->log(
						LogLevel::ERROR,
						'The DELETE method is not permitted for the given path.',
						[
							'httpMethod' => $httpMethod,
							'identifier' => $identifier,
							'apiKey' => $apiKey,
							'requestSegments' => $this->requestSegments,
							'requestHeaders' => $this->requestHeaders,
							'className' => $className,
							'actionName' => $actionName
						]
					);
				}

				return $this->createExceptionJsonResponse(
					'The DELETE method is not permitted for the given path.',
					405
				);
			}

			if (
				($httpMethod === 'PUT' && $identifier <= 0 && !$httpPermissions['putWithIdentifier']) ||
				($httpMethod === 'PATCH' && $identifier <= 0 && !$httpPermissions['patchWithIdentifier']) ||
				($httpMethod === 'POST' && $identifier > 0 && !$httpPermissions['postWithIdentifier'])
			) {
				if (((int) $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['sg_rest']['enableLogging']) === 1) {
					$this->logger->log(
						LogLevel::ERROR,
						'The ' . $httpMethod . ' method is not permitted for the given path.',
						[
							'httpMethod' => $httpMethod,
							'identifier' => $identifier,
							'apiKey' => $apiKey,
							'requestSegments' => $this->requestSegments,
							'requestHeaders' => $this->requestHeaders,
							'className' => $className,
							'actionName' => $actionName
						]
					);
				}

				return $this->createExceptionJsonResponse(
					'The ' . $httpMethod . ' method is not permitted for the given path.',
					405
				);
			}

			if (!class_exists($className)) {
				if (((int) $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['sg_rest']['enableLogging']) === 1) {
					$this->logger->log(
						LogLevel::ERROR,
						'The requested path does not exist.',
						[
							'httpMethod' => $httpMethod,
							'identifier' => $identifier,
							'apiKey' => $apiKey,
							'requestSegments' => $this->requestSegments,
							'requestHeaders' => $this->requestHeaders,
							'className' => $className,
							'actionName' => $actionName
						]
					);
				}

				return $this->createExceptionJsonResponse('The requested path does not exist.', 404);
			}

			if ($actionName !== '' && !method_exists($className, $actionName . 'Action')) {
				if (((int) $GLOBALS['TYPO3_CONF_VARS']['EXTENSIONS']['sg_rest']['enableLogging']) === 1) {
					$this->logger->log(
						LogLevel::ERROR,
						'The requested path does not exist.',
						[
							'httpMethod' => $httpMethod,
							'identifier' => $identifier,
							'apiKey' => $apiKey,
							'requestSegments' => $this->requestSegments,
							'requestHeaders' => $this->requestHeaders,
							'className' => $className,
							'actionName' => $actionName
						]
					);
				}

				return $this->createExceptionJsonResponse('The requested path does not exist.', 404);
			}
		}

		// The request is authenticated, the dispatcher now handles the actual initialization of the rest plugin
		return $handler->handle($request);
	}
}
