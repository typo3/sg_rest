<?php

use SGalinski\SgRest\Middleware\RestAuthenticator;
use SGalinski\SgRest\Middleware\RestDispatcher;

return [
	'frontend' => [
		// register the REST dispatcher middleware
		'sgalinski/sg-rest/dispatcher' => [
			'target' => RestDispatcher::class,
			'description' => 'Dispatches a REST request',
			'after' => [
				'typo3/cms-frontend/site'
			],
			'before' => [
				'typo3/cms-frontend/eid'
			]
		],
		// register the REST authenticator middleware
		'sgalinski/sg-rest/authenticator' => [
			'target' => RestAuthenticator::class,
			'description' => 'Authenticates a REST request',
			'before' => [
				'sgalinski/sg-rest/dispatcher'
			]
		]
	]
];
