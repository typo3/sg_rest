<?php

use TYPO3\CMS\Scheduler\Task\TableGarbageCollectionTask;

call_user_func(
	static function () {
		$GLOBALS['TYPO3_CONF_VARS']['SC_OPTIONS']['scheduler']['tasks'][TableGarbageCollectionTask::class]['options']['tables']['tx_sgrest_log'] = [
			'dateField' => 'time_micro',
			'expirePeriod' => 30
		];
	}
);
