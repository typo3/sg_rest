CREATE TABLE fe_users (
	tx_sgrest_auth_token    varchar(40)         DEFAULT ''  NOT NULL,
	tx_sgrest_access_groups varchar(255)        DEFAULT ''  NOT NULL,
	tx_sgrest_test_mode     tinyint(4) unsigned DEFAULT '0' NOT NULL
);

CREATE TABLE tx_sgrest_log (
	uid        int(11) unsigned                NOT NULL auto_increment,
	pid        int(11) unsigned    DEFAULT '0' NOT NULL,
	tstamp     int(11) unsigned    DEFAULT '0' NOT NULL,
	request_id varchar(13)         DEFAULT ''  NOT NULL,
	time_micro int(11) unsigned    DEFAULT '0' NOT NULL,
	component  varchar(255)        DEFAULT ''  NOT NULL,
	level      tinyint(1) unsigned DEFAULT '0' NOT NULL,
	message    text,
	data       text,
	PRIMARY KEY (uid),
	KEY parent(pid),
	KEY request(request_id)
);
