## Version 6 Breaking Changes:

- Dropped TYPO3 10 and 11 support
- Dropped PHP 7 support

## Version 5 Breaking Changes:

- Dropped TYPO3 9 Support
- Dropped php 7.3 Support

Instead of using an entity name for Plugin registration, we will use the corresponding
controller for registration, which will be added under classFQN key inside the Entity Configuration:

```php
$restRegistrationService->registerAccessGroup(
    'authentication',
    'SGalinski.sg_rest',
    'Authentication',
    [
       	'authentication' => [
       	    // Add THIS
            'classFQN' => \SGalinski\SgRest\Controller\Rest\Authentication\AuthenticationController::class,
            'read' => 'uid, title'
        ]
    ]
);
```

## Version 4 Breaking Changes:

- changed calling URL, either adapt your htaccess or your url.

OLD:

```apacheconf
RewriteRule ^api/v1/(.*) /index.php?type=1595576052&request=$1 [QSA]
```

NEW:

```apacheconf
RewriteRule ^api/v1/(.*) /index.php?type=1595576052&tx_sgrest[request]=$1 [QSA]
```

## Version 3 Breaking Changes

- Dropped TYPO3 9 support
- To optimize performance, the frontendUserRepository was dropped and repository-calls were replaced with direct
  $queries.
  Hence, the $authenticatedUser variable used in Middlewares, Services and sometime REST-Controllers is now an array.
  Therefore using the variable as an object doesn't work any longer and needs to be adjusted in external extensions.

```
Example:

OLD:
$authenticatedUser->getAuthToken()

NEW:
$authenticatedUser['tx_sgrest_auth_token']
```
