<?php

$EM_CONF['sg_rest'] = [
	'title' => 'REST API Base',
	'description' => 'The extension provides a basic REST environment.',
	'category' => 'misc',
	'author' => 'Stefan Galinski',
	'author_email' => 'stefan@sgalinski.de',
	'author_company' => 'sgalinski Internet Services',
	'state' => 'stable',
	'version' => '6.0.1',
	'constraints' => [
		'depends' => [
			'typo3' => '12.4.0-12.4.99',
			'php' => '8.1.0-8.3.99',
		],
		'conflicts' => [],
		'suggests' => [],
	],
];
