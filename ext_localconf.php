<?php

use Psr\Log\LogLevel;
use SGalinski\SgRest\Controller\Rest\Authentication\AuthenticationController;
use SGalinski\SgRest\Middleware\RestDispatcher;
use SGalinski\SgRest\Service\RegistrationService;
use TYPO3\CMS\Core\Log\Writer\FileWriter;
use TYPO3\CMS\Core\Utility\ExtensionManagementUtility;
use TYPO3\CMS\Core\Utility\GeneralUtility;
use TYPO3\CMS\Extbase\Utility\ExtensionUtility;

call_user_func(
	function ($extKey) {
		// Configure logger for the service controllers
		$GLOBALS['TYPO3_CONF_VARS']['LOG']['SGalinski']['SgRest']['writerConfiguration'] = [
			LogLevel::EMERGENCY => [
				FileWriter::class => ['logFileInfix' => 'tx_sgrest']
			],
			LogLevel::ALERT => [
				FileWriter::class => ['logFileInfix' => 'tx_sgrest']
			],
			LogLevel::CRITICAL => [
				FileWriter::class => ['logFileInfix' => 'tx_sgrest']
			],
			LogLevel::ERROR => [
				FileWriter::class => ['logFileInfix' => 'tx_sgrest']
			],
			LogLevel::WARNING => [
				FileWriter::class => ['logFileInfix' => 'tx_sgrest']
			],
			LogLevel::NOTICE => [
				FileWriter::class => ['logFileInfix' => 'tx_sgrest']
			],
			LogLevel::INFO => [
				FileWriter::class => ['logFileInfix' => 'tx_sgrest']
			],
			LogLevel::DEBUG => [
				FileWriter::class => ['logFileInfix' => 'tx_sgrest']
			]
		];

		ExtensionManagementUtility::addTypoScriptConstants(
			'plugin.sg_rest.pageType = ' . RestDispatcher::restPageType
		);

		ExtensionManagementUtility::addTypoScriptSetup(
			'@import "EXT:sg_rest/Configuration/TypoScript/setup.typoscript"'
		);

		/* REST Authentication Endpoint registration */
		$restRegistrationService = GeneralUtility::makeInstance(
			RegistrationService::class
		);

		$restRegistrationService->registerAccessGroup(
			'authentication',
			'SGalinski.sg_rest',
			'Authentication',
			[
				'authentication' => [
					'classFQN' => AuthenticationController::class,
					'read' => 'uid, title'
				]
			]
		);

		ExtensionUtility::configurePlugin('SgRest', 'Restauthentication', [
			AuthenticationController::class => 'postGetbearertoken'
		]);
	},
	'sg_rest'
);
